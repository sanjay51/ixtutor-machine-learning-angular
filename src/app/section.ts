import { DataService } from './data-service/data.service';
export abstract class Section {
    constructor(protected dataService: DataService) {
        dataService.registerSection(this.getSectionId(), this.getTitle());
    }
    abstract getSectionId(): string;
    abstract getTitle(): string;
}
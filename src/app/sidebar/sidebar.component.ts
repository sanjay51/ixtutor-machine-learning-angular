import { faSquare } from '@fortawesome/free-solid-svg-icons';
import { LearnTensorflowMetadata } from '../tutorial/learn-tensorflow/learn-tensorflow-metadata';
import { DataService } from '../data-service/data.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { StateService } from '../state-service/state.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  faSquare = faSquare;
  
  selectedPartId = 1;

  constructor(private dataService: DataService,
    private cdref: ChangeDetectorRef,
    private state: StateService) { }

  ngOnInit() {
    this.state.getActivePartId().subscribe(a => {
      this.selectedPartId = a;
      this.cdref.detectChanges()
    });
  }

  getPartIds(): number[] {
    return this.dataService.getLearnTensorflowMetadata().getPartIds();
  }

  isPartHighlighted(partId: number) {
    return this.getPartMetadata(partId).highlight;
  }

  getPartMetadata(partId: number) {
    return this.dataService.getLearnTensorflowMetadata().getPartMetadata(partId);
  }

  getPartTitle(partId: number) {
    let partMetadata = this.getPartMetadata(partId);
    if (!partMetadata) return "";
    return partMetadata.title;
  }

  getRouterLink(partId: number) {
    let path = this.dataService.getLearnTensorflowMetadata().getPartMetadata(partId).path;
    return "/" + LearnTensorflowMetadata.getPath() + "/" + partId + "/" + path + ".html";
  }

  partIdLoadEvent(partId: number) {
    this.state.setActivePartId(partId);
  }

  getMenuItemClass(partId: number) {
    if (partId == this.selectedPartId) {
      return 'row sidebar-element sidebar-menu-item-selected';
    }
    return 'row sidebar-element sidebar-menu-item';
  }

}

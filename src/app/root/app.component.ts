import { StateService } from './../state-service/state.service';
import { RoutingService } from './../routing.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ixtutor-machine-learning';

  constructor(private router: Router, 
    private state: StateService, private routing: RoutingService) { }

  ngOnInit() {

    this.router.events.subscribe(e => {
      let sectionId = this.routing.getSectionIdFromRoute();
      this.state.setActiveSection(sectionId);
    });
  }

  showSidebar() {
    return this.routing.isTutorialPage();
  }

  getClass() {
    return this.showSidebar() ? 'col-lg-9' : 'col-lg-12';
  }
}


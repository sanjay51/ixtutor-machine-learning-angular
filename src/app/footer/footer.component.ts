import { faEnvelope, faGlobe, faAt } from '@fortawesome/free-solid-svg-icons';
import { faFacebookSquare, faTwitterSquare, faRedditSquare } from '@fortawesome/free-brands-svg-icons';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  faEnvelope = faEnvelope;
  faGlobe = faGlobe;
  faRedditSquare = faRedditSquare;
  faAt = faAt;

  faFacebookSquare = faFacebookSquare;
  faTwitterSquare = faTwitterSquare;

  constructor() { }

  ngOnInit() {
  }

}

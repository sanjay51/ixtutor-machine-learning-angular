export interface Metadata {
    getTitle(): string;
    getPartMetadata(partId: number): any;
    getPartIds(): number[];
    getMaxPartId(): number;
}
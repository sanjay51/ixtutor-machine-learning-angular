import { HttpClient } from '@angular/common/http';
import { LearnTensorflowMetadata } from './../tutorial/learn-tensorflow/learn-tensorflow-metadata';
import { Metadata } from './metadata';
import { LearnTensorflowComponent } from '../tutorial/learn-tensorflow/learn-tensorflow.component';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  data = {
    'learn-tensorflow': new LearnTensorflowMetadata()
  }

  sectionMetadata = {};

  constructor(private http: HttpClient) { }

  public getTutorialMetadata(tutorialId: string): Metadata {
    return this.data[tutorialId];
  }

  public getLearnTensorflowMetadata(): Metadata {
    return this.data[LearnTensorflowMetadata.getPath()];
  }

  public registerSection(sectionId, title) {
    this.sectionMetadata[sectionId] = title;
  }

  public getTitleBySectionId(sectionId) {
    return this.sectionMetadata[sectionId];
  }

  /**
   * 
   * @param tutorialId The tutorial id, e.g. "learn-tensorflow"
   * @param partId The part id, e.g. 1, or 2
   */
  public getTutorialHTML(tutorialId: string, partId: string) {
    let id = "tutorial_" + tutorialId + "_" + partId;
    console.log("here");
    return this.http.get("/live-editor.html");
  }
}


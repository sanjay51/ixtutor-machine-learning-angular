import { StateService } from './../state-service/state.service';
import { Component, OnInit } from '@angular/core';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { DEFAULT_PATH_LEARN_TENSORFLOW } from '../routing.service';

@Component({
    selector: 'my-header',
    styleUrls: ['./header.component.scss'],
    templateUrl: './header.component.html'
})

export class HeaderComponent implements OnInit {
    title = 'ixTutor';
    selectedAlias = 'learn-tensorflow';
    faHome = faHome;

    constructor(private state: StateService) { }

    ngOnInit() {
        this.state.getActiveSection().subscribe(a => this.selectedAlias = a);
    }

    gotoHome() {
        
    }

    getClass(alias) {
        if (alias == this. selectedAlias) return "current";

        return "";
    }

    getRouterLink(alias) {
        if (alias == '') return alias;
        if (alias == 'learn-tensorflow') return DEFAULT_PATH_LEARN_TENSORFLOW + ".html";

        return "/" + alias + ".html";
    }

    sectionLoadEvent(alias) {
        this.state.setActiveSection(alias);
    }

}
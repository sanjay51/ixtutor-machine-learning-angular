import { Component, OnInit } from '@angular/core';
import { StateService } from '../state-service/state.service';
import { Section } from '../section';
import { DataService } from '../data-service/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends Section implements OnInit {
  getSectionId(): string {
    return "home"
  }
  getTitle(): string {
    return "";
  }

  constructor(private state: StateService, dataService: DataService) { super(dataService) }

  ngOnInit() {
  }

  getRouterLink(alias) {
    return alias + ".html";
  }
}

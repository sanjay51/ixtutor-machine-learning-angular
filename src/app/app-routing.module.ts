import { ExternalRouterComponent } from './external-router.component';
import { HomeComponent } from './home/home.component';
import { DeepLearningComponent } from './tutorial/deep-learning/deep-learning.component';
import { AboutComponent } from './about/about.component';
import { LiveEditorComponent } from './live-editor/live-editor.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ROUTE_ABOUT, ROUTE_LIVE_EDITOR, ROUTE_LEARN_TENSORFLOW, DEFAULT_PATH_LEARN_TENSORFLOW, ROUTE_DEEP_LEARNING } from './routing.service';

const HTML = ".html";

const routes: Routes = [

  { 
    path: '',
    component: HomeComponent
  },

  { 
    path: 'home' + HTML,
    component: HomeComponent
  },

  { 
    path: ROUTE_LEARN_TENSORFLOW + HTML,
    redirectTo: DEFAULT_PATH_LEARN_TENSORFLOW + HTML,
    pathMatch: 'full'
  },

  { 
    path: ROUTE_DEEP_LEARNING + HTML,
    component: DeepLearningComponent
  },

  {
    path: ROUTE_LIVE_EDITOR + HTML,
    component: LiveEditorComponent
  },

  {
    path: ROUTE_ABOUT + HTML,
    component: AboutComponent
  },

  {
    path: ROUTE_LIVE_EDITOR,
    component: LiveEditorComponent
  },

  {
    path: ROUTE_LIVE_EDITOR + HTML,
    component: LiveEditorComponent
  },

  {
    path: 'linear-regression-with-tensorflow-with-interactive-code-examples',
    redirectTo: 'learn-tensorflow/5/linear_regression_with_tensorflow' + HTML
  },
  
  {
    path: 'learning-tensorflow-by-example-part-1-basics',
    redirectTo: 'learn-tensorflow/1/introduction' + HTML
  },
  
  {
    path: 'tensorflow-basics-with-housing-prices-prediction-example-chapter-i',
    redirectTo: 'learn-tensorflow/1/introduction' + HTML
  },
  
  {
    path: 'support-vector-machines-svm',
    component: ExternalRouterComponent
  },
  
  {
    path: 'curse-of-dimensionality-and-the-truths-about-data',
    component: ExternalRouterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Constants } from './../constants';
import { DataService } from './../data-service/data.service';
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class StateService {
  tutorial: string;
  partId = new Subject<number>();
  section = new Subject<string>();

  constructor(private titleService: Title, private dataService: DataService) {
    this.partId.asObservable().subscribe(partId => {
      let tutorialTitle = this.dataService.getLearnTensorflowMetadata().getTitle();
      let partTitle = this.dataService.getLearnTensorflowMetadata().getPartMetadata(partId).title;
      
      this.titleService.setTitle(partTitle + " - " + tutorialTitle + " - " + Constants.SITE_TITLE);
    });

    this.section.asObservable().subscribe(sectionId => {
      let sectionTitle = this.dataService.getTitleBySectionId(sectionId);
      if (sectionTitle) sectionTitle = sectionTitle + " - ";
      else sectionTitle = "";
      this.titleService.setTitle(sectionTitle + Constants.SITE_TITLE);
    })
  }

  getActiveTutorial(): string {
    return this.tutorial;
  }

  setActiveTutorial(tutorial: string) {
    this.tutorial = tutorial;
  }

  setActivePartId(partId: number) {
    this.partId.next(partId);
    window.scroll(0, 0);
  }

  getActivePartId(): Observable<number> {
    return this.partId.asObservable();
  }

  setActiveSection(section: string) {
    this.section.next(section);
  }

  getActiveSection(): Observable<string> {
    return this.section.asObservable();
  }
}

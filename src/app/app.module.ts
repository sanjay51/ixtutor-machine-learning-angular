import { NoteComponent } from './tutorial/element/note/note.component';
import { QuestionAnswerComponent } from './tutorial/element/question-answer/question-answer.component';
import { CodeRunComponent } from './tutorial/element/code-run/code-run.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './root/app.component';

import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LearnTensorflowComponent } from './tutorial/learn-tensorflow/learn-tensorflow.component';
import { LearnTensorflowRoutingModule } from './tutorial/learn-tensorflow/learn-tensorflow-routing.module';
import { FooterComponent } from './footer/footer.component';
import { LiveEditorComponent } from './live-editor/live-editor.component';
import { AboutComponent } from './about/about.component';
import { DeepLearningComponent } from './tutorial/deep-learning/deep-learning.component';
import { HomeComponent } from './home/home.component';
import { NgModule, NO_ERRORS_SCHEMA, Injector } from '@angular/core';
import { SharedModule } from './shared.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import {createCustomElement} from '@angular/elements';
import { NewsletterFormComponent } from './newsletter-form/newsletter-form.component';
import { ExternalRouterComponent } from './external-router.component';
import { PostHeadingComponent } from './tutorial/element/post-heading/post-heading.component';
import { AceEditorModule } from 'ng2-ace-editor';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    LearnTensorflowComponent,
    FooterComponent,
    LiveEditorComponent,
    AboutComponent,
    DeepLearningComponent,
    HomeComponent,
    NewsletterFormComponent,
    ExternalRouterComponent,
    PostHeadingComponent,
    NoteComponent,
    QuestionAnswerComponent,
    CodeRunComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    LearnTensorflowRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    FontAwesomeModule,
    HttpClientModule,
    HttpClientJsonpModule,
    AceEditorModule
  ],
  exports: [
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [
      NO_ERRORS_SCHEMA
  ],
  entryComponents: [PostHeadingComponent, NoteComponent, QuestionAnswerComponent, CodeRunComponent]
})
export class AppModule {
  constructor(injector: Injector) {
    const postHeadingElement = createCustomElement(PostHeadingComponent, {injector: injector});
    customElements.define('post-heading', postHeadingElement);

    const noteElement = createCustomElement(NoteComponent, {injector: injector});
    customElements.define('ix-note', noteElement);

    const qaElement = createCustomElement(QuestionAnswerComponent, {injector: injector});
    customElements.define('question-answer', qaElement);

    const codeRunElement = createCustomElement(CodeRunComponent, {injector: injector});
    customElements.define('code-run', codeRunElement);
  }
}

import { RoutingService } from './routing.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-external-router',
  template: 'Redirecting to old ixTutor..'
})
export class ExternalRouterComponent implements OnInit {

  constructor(private routing: RoutingService) { }

  ngOnInit() {
      this.routing.identifyAndRedirectToExternal();
  }

}

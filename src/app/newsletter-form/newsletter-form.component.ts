import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpParams } from '@angular/common/http';
import { faAt, faCog } from '@fortawesome/free-solid-svg-icons';

interface MailChimpResponse {
  result: string;
  msg: string;
}

@Component({
  selector: 'app-newsletter-form',
  templateUrl: './newsletter-form.component.html',
  styleUrls: ['./newsletter-form.component.scss']
})
export class NewsletterFormComponent implements OnInit {
  faAt = faAt;
  faCog = faCog;
  SubscriptionStatus = SubscriptionStatus;

  status = SubscriptionStatus.INITIAL; //'working', 'subscribed'

  newsletterForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email])
  })

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  submitted = false;
  mailChimpEndpoint = 'https://ixtutor.us20.list-manage.com/subscribe/post-json?u=e12aa6149158ec131ea29e8b1&id=8d2182dc53';
  error = '';

  onSubmit() {
    this.error = '';
    if (this.newsletterForm.status !== 'VALID') {
      alert('Invalid email address.');
      return;
    }

    const params = new HttpParams()
      .set('EMAIL', this.newsletterForm.get('email').value)

    const mailChimpUrl = this.mailChimpEndpoint + '&' + params.toString();

    this.status = SubscriptionStatus.WORKING;
    // 'c' refers to the jsonp callback param key. This is specific to Mailchimp
    this.http.jsonp<MailChimpResponse>(mailChimpUrl, 'c').subscribe(response => {
      this.status = SubscriptionStatus.SUBSCRIBED;
      if (response.result && response.result !== 'error') {
        this.submitted = true;
        this.status = SubscriptionStatus.SUBSCRIBED;
      }
      else {
        this.error = response.msg;
        this.status = SubscriptionStatus.ERROR;
      }
    }, error => {
      console.error(error);
      this.error = 'Sorry, an error occurred.';
      this.status = SubscriptionStatus.ERROR;
    });
  }

  reset() {
    this.status = SubscriptionStatus.INITIAL;
    this.newsletterForm.reset();
  }

  retry() {
    this.status = SubscriptionStatus.WORKING;
    this.onSubmit();
  }

}

export enum SubscriptionStatus {
  INITIAL,
  WORKING,
  SUBSCRIBED,
  ERROR
}
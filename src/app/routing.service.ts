import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RoutingService {

  constructor(private route: ActivatedRoute) { }

  public isTutorialPage() {
    return !(this.isLiveEditorPage() || 
      this.isAboutPage() || 
      this.isDeepLearningPage() ||
      this.isHomePage()
    );
  }
  public isLiveEditorPage() {
    return (window.location.pathname == '/' + ROUTE_LIVE_EDITOR + HTML) ||
    (window.location.pathname == '/' + ROUTE_LIVE_EDITOR);
  }

  public isAboutPage() {
    return (window.location.pathname == '/' + ROUTE_ABOUT + HTML);
  }

  public isDeepLearningPage() {
    return (window.location.pathname == '/' + ROUTE_DEEP_LEARNING + HTML);
  }

  public isHomePage() {
    return (window.location.pathname == '/' || window.location.pathname == '/home' + HTML);
  }

  getSectionIdFromRoute() {
    let primaryRoute = this.route.snapshot.children[0];
    if (primaryRoute == null) return ROUTE_HOME;

    let path = primaryRoute.routeConfig.path;
    let sectionId = path.replace(".html", "");

    if (sectionId == '') return ROUTE_HOME;
    return sectionId;
  }

  getPartIdFromRoute() {
    return +this.route.snapshot.children[0].routeConfig.path.split("/")[0];
  }

  identifyAndRedirectToExternal() {
    let path = this.route.snapshot.children[0].routeConfig.path
    window.location.href = "https://old.ixtutor.com/" + path + "/";
  }
}

const HTML = ".html";
export const ROUTE_HOME = "home";
export const ROUTE_LEARN_TENSORFLOW = "learn-tensorflow";
export const ROUTE_DEEP_LEARNING = "deep-learning";
export const ROUTE_ABOUT = "about";
export const ROUTE_LIVE_EDITOR = "live-editor";

export const DEFAULT_PATH_LEARN_TENSORFLOW = '/' + ROUTE_LEARN_TENSORFLOW + '/1/introduction';
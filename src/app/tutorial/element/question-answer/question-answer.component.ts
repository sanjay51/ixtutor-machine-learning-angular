import { Component, OnInit, Input } from '@angular/core';

@Component({
  templateUrl: './question-answer.component.html',
  styleUrls: ['./question-answer.component.scss']
})
export class QuestionAnswerComponent implements OnInit {
  @Input() question;
  @Input() options;
  @Input() answer: number;
  @Input('answer-explanation') answerExplanation;

  public isCorrectAnswer = false;
  public isDirty = false;

  constructor() { }

  ngOnInit() {
  }

  getQuestion() {
    return this.question;
  }

  getOption(option) {
    if (this.options == undefined) return "";
    return this.options[option-1];
  }

  getAnswerExplanation() {
    return this.answerExplanation;
  }

  evaluateOption(option: number) {
    this.isDirty = true;

    if (option == this.answer) 
      this.isCorrectAnswer = true;
    else this.isCorrectAnswer = false;
  }

}

import { Component, OnInit, Input } from '@angular/core';

@Component({
  templateUrl: './post-heading.component.html',
  styleUrls: ['./post-heading.component.scss']
})
export class PostHeadingComponent implements OnInit {
  @Input() text;

  constructor() { }

  ngOnInit() {
  }

  getText() {
    return this.text;
  }

}

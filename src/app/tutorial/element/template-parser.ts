export class TemplateParser {

    public parse(raw: string): TemplateParseResult {

        let parseResult: TemplateParseResult = new TemplateParseResult(raw, false);
        
        parseResult.parsedTemplate = "<body>" + raw + "</body>";

        return parseResult;
    }
}

export class TemplateParseResult {
    error: boolean = false;
    parsedTemplate: string;

    constructor(parsedTemplate: string, error: boolean) {
        this.parsedTemplate = parsedTemplate;
        this.error = error;
    }

    isError(): boolean {
        return this.error;
    }

    getParsedTemplate() {
        return this.parsedTemplate;
    }
}

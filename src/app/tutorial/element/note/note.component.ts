import { Component, OnInit, Input } from '@angular/core';

@Component({
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit {
  @Input() title;
  @Input() text;

  constructor() { }

  ngOnInit() {
  }

  getTitle() {
    if (!this.title) return "";
    return this.title + ": ";
  }

  getText() {
    return this.text;
  }

}

import { LearnTensorflowMetadata } from '../learn-tensorflow-metadata';
import { DataService } from '../../../data-service/data.service';
import { Tutorial } from '../../tutorial';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-t2',
  templateUrl: './p2.component.html',
  styleUrls: ['./p2.component.scss']
})
export class P2Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}

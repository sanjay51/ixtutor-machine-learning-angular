<post-heading text="We'll learn how a powerful statistical learning technique benefits from the efficiency and flexibility of Tensforflow."></post-heading>

We’ll learn about,
<ul>
  <li>implementing linear regression with tensorflow (using normal equation)</li>
  <li>implementing gradient descent (manually computing gradients)</li>
  <li>using tensorflow autodiff to automatically compute gradients</li>
  <li>using optimizers</li>
</ul>

<h3>Linear regression</h3>Linear regression is a machine learning technique in the form of a linear equation, which takes in a set of input features, and produces an output,
<img src="assets/images/linear_regression/p5-linear-regression-1.png" style="width: 50%;" />
Here x1, x2, … xn are the input features, and y-hat is the predicted output. θ0, θ1, … θn are the set of parameters that are determined during the training based on the training data.

In vector form,
<img src="assets/images/linear_regression/p5-linear-regression-2.png" style="width:131px; height:39px"/>
We'll discuss three techniques of linear regression with Tensorflow.
<br/>
<h4>1. Using normal equation technique</h4>We’ll first use a statistical technique called Normal Equation for determining the optimal θ vector (with Tensorflow).
<img src="assets/images/linear_regression/p5-linear-regression-3.png" style="width:233px; height:54px;"/>
<br/>
<b>Code example</b>: We’ll predict use the California housing prices dataset.
<code-run pre-code="" code="import numpy as np
from sklearn.datasets import fetch_california_housing

# Download the data. California housing is a standard sklearn dataset, so we'll just use it from there.
housing = fetch_california_housing()
m, n = housing.data.shape

# Add a bias column (with all ones)
housing_data_with_bias = np.c_[np.ones((m, 1)), housing.data]

# Initialize X and y constants in tensorflow
X = tf.constant(housing_data_with_bias, dtype = tf.float32, name='X')
y = tf.constant(housing.target.reshape(-1, 1), dtype = tf.float32, name='y')

# Define the value of theta with normal equation
XT = tf.transpose(X)
XTdotX = tf.matmul(XT, X)
XTdotX_inverse = tf.matrix_inverse(XTdotX)
XTdotY = tf.matmul(XT, y)
theta = tf.matmul(XTdotX_inverse, XTdotY)

# Evaluate theta
with tf.Session() as sess:
    theta_value = theta.eval()

print(theta_value)" post-code="" output="[[ -3.72148438e+01]
[ 4.36286926e-01]
[ 9.39083099e-03]
[ -1.07070923e-01]
[ 6.44622803e-01]
[ -4.04566526e-06]
[ -3.78060341e-03]
[ -4.23736572e-01]
[ -4.37438965e-01]]"></code-run>
<ix-note title="Note" text="You can also run normal equation directly using NumPy, the advantage with Tensorflow will be that it will automatically run it on GPU if your computer have one."></ix-note>Normal equation is great, however it doesn’t scale well when we have large no. of features. The computational complexity of computing (XT . X)-1 is typically between O(n2.4) to O(n3) depending on the implementation (n = no. of features), which easily becomes an issue when n is large.

<question-answer question="Normal equation algorithm is efficient when we have a lot of training instances, but very less features." [options]="['True', 'False', 'Unrelated', 'Sometimes']" answer=1 answer-explanation="Computing Normal equation scales linearly with no. of training instances."></question-answer>In the next section, we’ll discuss gradient descent technique that allow efficient scaling with number of features.

<h4>2. Linear regression with gradient descent (manually computing gradients)</h4>

We’ll use batch gradient descent, manually computing the gradients.

Remember to first normalize the input feature vectors when using gradient descent, otherwise the training may be slow.

<code-run pre-code="" code="import numpy as np
from sklearn.datasets import fetch_california_housing

# Download the data. California housing is a standard sklearn dataset, so we'll just use it from there.
housing = fetch_california_housing()
m, n = housing.data.shape

# Add a bias column (with all ones)
housing_data_with_bias = np.c_[np.ones((m, 1)), housing.data]

# Normalize input features
from sklearn.preprocessing import StandardScaler
housing_data_with_bias_scaled = StandardScaler().fit_transform(housing_data_with_bias)

n_epochs = 1000
learning_rate = 0.01

# Define X, y, theta
X = tf.constant(housing_data_with_bias_scaled, dtype = tf.float32, name = 'X')
y = tf.constant(housing.target.reshape(-1, 1), dtype = tf.float32, name = 'y')
theta = tf.Variable(tf.random_uniform([n+1, 1], -1.0, 1.0), name = 'theta')
y_prediction = tf.matmul(X, theta, name = 'y_prediction')

# Compute mean squared error
error = y_prediction - y
mse = tf.reduce_mean(tf.square(error), name = 'mse')

# Compute gradients
gradients = 2/m * tf.matmul(tf.transpose(X), error)

# Update theta
theta_new = theta - learning_rate * gradients
theta_update_op = tf.assign(theta, theta_new)

init = tf.global_variables_initializer()

# Run
with tf.Session() as sess:
    sess.run(init)
    for epoch in range(n_epochs):
        if epoch % 100 == 0:
            print('Epoch', epoch, 'MSE =', mse.eval())
        sess.run(theta_update_op)

    best_theta = theta.eval()

print(best_theta)" post-code="" output="Epoch 0 MSE = 8.89834
Epoch 100 MSE = 4.87219
Epoch 200 MSE = 4.80936
Epoch 300 MSE = 4.80671
Epoch 400 MSE = 4.80572
Epoch 500 MSE = 4.80504
Epoch 600 MSE = 4.80455
Epoch 700 MSE = 4.80419
Epoch 800 MSE = 4.80393
Epoch 900 MSE = 4.80375
[[-0.20699835]
 [0.81415182]
 [0.12314661]
 [-0.2226586]
 [0.26424694]
 [-0.00270369]
 [-0.03936818]
 [-0.88236874]
 [-0.85056376]]"></code-run>

This works fine, however it requires us to manually compute the gradients from the MSE cost function. Manually computing gradients is easy for linear regression, but for more complicated machine learning algorithms (e.g. neural networks), it can really be tedious and error prone.

In the next section, we’ll use a Tensorflow’s autodiff  technique to compute the gradients.

<h4>3. Computing gradients using Tensorflow’s “autodiff” </h4>

Consider the function,

f(x) = exp(exp(exp(x)))

For computing gradients, we require its derivative,

f'(x) = exp(x) * exp(exp(x)) * exp(exp(exp(x)))

There are two way of computing f'(x)
<li>Compute a = exp(x), b = exp(exp(x)), c = exp(exp(exp(x)), then finally f'(x) = a * b * c. The problem is that we have to compute exp(x) thrice, exp(exp(x)) twice, eventually executing the exp function nine times, which is inefficient and can be avoided if we reuse the values once computed.</li>
<li>Or, compute as, a = exp(x), then b = exp(b), then c = exp(c), and finally f'(x) = a * b * c. This is efficient because we reuse the values once computed (exp function is executed only three times).</li>

The first method above is called symbolic differentiation, which is clearly not efficient.

Tensorflow computation happens in the second way, which is what autodiff feature is about. It just reuse the values does not compute them again.

<code-run pre-code="import numpy as np
from sklearn.datasets import fetch_california_housing

# Download the data. California housing is a standard sklearn dataset, so we'll just use it from there.
housing = fetch_california_housing()
m, n = housing.data.shape

# Add a bias column (with all ones)
housing_data_with_bias = np.c_[np.ones((m, 1)), housing.data]

# Normalize input features
from sklearn.preprocessing import StandardScaler
housing_data_with_bias_scaled = StandardScaler().fit_transform(housing_data_with_bias)

n_epochs = 1000
learning_rate = 0.01

# Define X, y, theta
X = tf.constant(housing_data_with_bias_scaled, dtype = tf.float32, name = 'X')
y = tf.constant(housing.target.reshape(-1, 1), dtype = tf.float32, name = 'y')
theta = tf.Variable(tf.random_uniform([n+1, 1], -1.0, 1.0), name = 'theta')
y_prediction = tf.matmul(X, theta, name = 'y_prediction')

# Compute mean squared error
error = y_prediction - y
mse = tf.reduce_mean(tf.square(error), name = 'mse')

" code="# Compute gradients w.r.t. theta with Tensorflow' autodiff
gradients = tf.gradients(mse, [theta])[0]

# Update theta
theta_new = theta - learning_rate * gradients
theta_update_op = tf.assign(theta, theta_new)" post-code="

init = tf.global_variables_initializer()

# Run
with tf.Session() as sess:
    sess.run(init)
    for epoch in range(n_epochs):
        if epoch % 100 == 0:
            print('Epoch', epoch, 'MSE =', mse.eval())
        sess.run(theta_update_op)

    best_theta = theta.eval()

print(best_theta)" output="Epoch 0 MSE = 7.984
Epoch 100 MSE = 5.02138
Epoch 200 MSE = 4.92467
Epoch 300 MSE = 4.8908
Epoch 400 MSE = 4.86722
Epoch 500 MSE = 4.85012
Epoch 600 MSE = 4.83767
Epoch 700 MSE = 4.82858
Epoch 800 MSE = 4.82194
Epoch 900 MSE = 4.81709
[[-0.05003357]
[0.72338957]
[0.13447441]
[0.00183858]
[0.05529203]
[0.00259521]
[-0.03839117]
[-0.88502127]
[-0.84008807]]"></code-run>

This is great, efficient. However we’re still computing and updating theta vector manually. We can use an optimizer instead which will compute both the gradients and the theta out of the box.

<h3>Using Optimizer instead of manually computing θ vector</h3>
<b>Code example</b>: We’ll use Gradient Descent optimizer.

<code-run pre-code="import numpy as np
from sklearn.datasets import fetch_california_housing

# Download the data. California housing is a standard sklearn dataset, so we'll just use it from there.
housing = fetch_california_housing()
m, n = housing.data.shape

# Add a bias column (with all ones)
housing_data_with_bias = np.c_[np.ones((m, 1)), housing.data]

# Normalize input features
from sklearn.preprocessing import StandardScaler
housing_data_with_bias_scaled = StandardScaler().fit_transform(housing_data_with_bias)

n_epochs = 1000
learning_rate = 0.01

# Define X, y, theta
X = tf.constant(housing_data_with_bias_scaled, dtype = tf.float32, name = 'X')
y = tf.constant(housing.target.reshape(-1, 1), dtype = tf.float32, name = 'y')
theta = tf.Variable(tf.random_uniform([n+1, 1], -1.0, 1.0), name = 'theta')
y_prediction = tf.matmul(X, theta, name = 'y_prediction')

# Compute mean squared error
error = y_prediction - y
mse = tf.reduce_mean(tf.square(error), name = 'mse')

" code="# Use an optimizer to perform gradient descent (compute gradients, compute and update theta values)
optimizer = tf.train.GradientDescentOptimizer(learning_rate = learning_rate)
training_op = optimizer.minimize(mse)" post-code="init = tf.global_variables_initializer()

# Run
with tf.Session() as sess:
    sess.run(init)
    for epoch in range(n_epochs):
        if epoch % 100 == 0:
            print('Epoch', epoch, 'MSE =', mse.eval())
        sess.run(theta_update_op)

    best_theta = theta.eval()

print(best_theta)" output="Epoch 0 MSE = 7.69663
Epoch 100 MSE = 7.69663
Epoch 200 MSE = 7.69663
Epoch 300 MSE = 7.69663
Epoch 400 MSE = 7.69663
Epoch 500 MSE = 7.69663
Epoch 600 MSE = 7.69663
Epoch 700 MSE = 7.69663
Epoch 800 MSE = 7.69663
Epoch 900 MSE = 7.69663
[[ 0.66144013]
[0.19855857]
[-0.60220885]
[-0.42816591]
[-0.77515912]
[-0.62202144]
[-0.5148325 ]
[0.42171621]
[-0.03213978]]"></code-run>

Tensorflow also provide other types of optimizers, e.g. MomentumOptimizer which converges much faster than Gradient descent.

<code-run pre-code="import numpy as np
from sklearn.datasets import fetch_california_housing

# Download the data. California housing is a standard sklearn dataset, so we'll just use it from there.
housing = fetch_california_housing()
m, n = housing.data.shape

# Add a bias column (with all ones)
housing_data_with_bias = np.c_[np.ones((m, 1)), housing.data]

# Normalize input features
from sklearn.preprocessing import StandardScaler
housing_data_with_bias_scaled = StandardScaler().fit_transform(housing_data_with_bias)

n_epochs = 1000
learning_rate = 0.01

# Define X, y, theta
X = tf.constant(housing_data_with_bias_scaled, dtype = tf.float32, name = 'X')
y = tf.constant(housing.target.reshape(-1, 1), dtype = tf.float32, name = 'y')
theta = tf.Variable(tf.random_uniform([n+1, 1], -1.0, 1.0), name = 'theta')
y_prediction = tf.matmul(X, theta, name = 'y_prediction')

# Compute mean squared error
error = y_prediction - y
mse = tf.reduce_mean(tf.square(error), name = 'mse')

" code="# Use an optimizer to perform gradient descent (compute gradients, compute and update theta values)
optimizer = tf.train.MomentumOptimizer(learning_rate = learning_rate, momentum = 0.9)
training_op = optimizer.minimize(mse)
" post-code="
init = tf.global_variables_initializer()

# Run
with tf.Session() as sess:
    sess.run(init)
    for epoch in range(n_epochs):
        if epoch % 100 == 0:
            print('Epoch', epoch, 'MSE =', mse.eval())
        sess.run(theta_update_op)

    best_theta = theta.eval()

print(best_theta)" output="Epoch 0 MSE = 11.9987
Epoch 100 MSE = 11.9987
Epoch 200 MSE = 11.9987
Epoch 300 MSE = 11.9987
Epoch 400 MSE = 11.9987
Epoch 500 MSE = 11.9987
Epoch 600 MSE = 11.9987
Epoch 700 MSE = 11.9987
Epoch 800 MSE = 11.9987
Epoch 900 MSE = 11.9987
[[-0.0038662 ]
[-0.95145798]
[0.93909073]
[0.5503068 ]
[0.13636994]
[-0.94808888]
[0.96955562]
[0.65538311]
[-0.02074265]]"></code-run>
import { P8TransferLearningTfhubComponent } from './p8-transfer-learning-tfhub/p8-transfer-learning-tfhub.component';
import { P7TransferLearningComponent } from './p7-transfer-learning/p7-transfer-learning.component';
import { SharedModule } from './../../shared.module';
import { P6NeuralNetworkComponent } from './p6-neural-network/p6-neural-network.component';
import { LearnTensorflowMetadata } from './learn-tensorflow-metadata';
import { LearnTensorflowComponent } from './learn-tensorflow.component';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { P1Component } from './p1/p1.component';
import { P2Component } from './p2/p2.component';
import { P3Component } from './p3/p3.component';
import { P4Component } from './p4/p4.component';
import { CommonModule } from '@angular/common'
import { P5LinearRegressionComponent } from './p5-linear-regression/p5-linear-regression.component';

const HTML = ".html";
const learnTensorflowModuleRoutes: Routes = [
    {
        path: LearnTensorflowMetadata.getPath(), //<---- parent component declared here
        component: LearnTensorflowComponent,
        children: [                              //<---- child components declared here
            {
                path: '1/:part' + HTML,
                component: P1Component,
            },
            {
                path: '2/:part',
                component: P2Component,
            },
            {
                path: '3/:part',
                component: P3Component,
            },
            {
                path: '4/:part',
                component: P4Component,
            },
            {
                path: '5/:part',
                component: P5LinearRegressionComponent,
            },
            {
                path: '6/:part',
                component: P6NeuralNetworkComponent,
            },
            {
                path: '7/:part',
                component: P7TransferLearningComponent,
            },
            {
                path: '8/:part',
                component: P8TransferLearningTfhubComponent,
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(learnTensorflowModuleRoutes),
        CommonModule,
        SharedModule
    ],
    declarations: [
        P1Component,
        P2Component,
        P3Component,
        P4Component,
        P5LinearRegressionComponent,
        P6NeuralNetworkComponent,
        P7TransferLearningComponent,
        P8TransferLearningTfhubComponent,
    ],
    exports: [
        RouterModule
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class LearnTensorflowRoutingModule { }
import { StateService } from './../../../state-service/state.service';
import { LearnTensorflowMetadata } from '../learn-tensorflow-metadata';
import { DataService } from '../../../data-service/data.service';
import { Tutorial } from '../../tutorial';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-p1',
  templateUrl: './p1.component.html',
  styleUrls: ['./p1.component.scss']
})
export class P1Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}

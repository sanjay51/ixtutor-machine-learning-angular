import { ROUTE_LIVE_EDITOR } from './../../routing.service';
import { faEnvelope, faEdit, faGripLinesVertical, faGripVertical } from '@fortawesome/free-solid-svg-icons';
import { faFacebookF, faTwitter, faLinkedin } from '@fortawesome/free-brands-svg-icons';
import { LearnTensorflowMetadata } from './learn-tensorflow-metadata';
import { DataService } from '../../data-service/data.service';
import { Component, OnInit } from '@angular/core';
import { StateService } from '../../state-service/state.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ChangeDetectorRef} from '@angular/core';
import { Section } from '../../section';

@Component({
  selector: 'app-learn-tensorflow',
  templateUrl: './learn-tensorflow.component.html',
  styleUrls: ['./learn-tensorflow.component.scss']
})
export class LearnTensorflowComponent extends Section implements OnInit {
  /* Inherited method */
  getSectionId(): string {
    return "learn-tensorflow"
  }

  path = LearnTensorflowMetadata.getPath();
  selectedPartId = 1;

  faFacebook = faFacebookF;
  faTwitter = faTwitter;
  faLinkedin = faLinkedin;
  faEnvelope = faEnvelope;
  faEdit = faEdit;
  faVertical = faGripVertical;

  constructor(private route: ActivatedRoute, private router: Router,
    private cdref: ChangeDetectorRef,
    dataService: DataService, private state: StateService) { super(dataService) }

  ngOnInit() {
    this.state.getActivePartId().subscribe(a => {
      this.selectedPartId = a;
      this.cdref.detectChanges();
    });

    this.state.setActivePartId(this.getPartIdFromRoute());

    this.router.events.subscribe(e => {
      this.state.setActivePartId(this.getPartIdFromRoute());
    });
  }

  getPartIdFromRoute() {
    return +this.route.snapshot.children[0].routeConfig.path.split("/")[0];
  }

  getTitle() {
    let partMetadata = this.dataService
      .getLearnTensorflowMetadata()
      .getPartMetadata(this.selectedPartId)

    if (!partMetadata) return "";
    
    return partMetadata.title;
  }

  isFirstPart() {
    return this.selectedPartId == 1;
  }

  isLastPart() {
    return this.selectedPartId == this.dataService.getLearnTensorflowMetadata().getMaxPartId();
  } 

  getNextLink() {
    let currentPartId = this.selectedPartId;

    if (currentPartId == null) return "/";
    let nextPartId = currentPartId + 1;

    if (nextPartId > this.dataService.getLearnTensorflowMetadata().getMaxPartId())
      return "/";
      
    let path = this.dataService.getLearnTensorflowMetadata().getPartMetadata(nextPartId).path;
    return "/" + LearnTensorflowMetadata.getPath() + "/" + nextPartId + "/" + path + ".html";
  }

  getPreviousLink() {
    let currentPartId = this.selectedPartId;
    if (currentPartId == null) return "/"
    let prevPartId = currentPartId - 1;

    if (prevPartId == 0) return "/";
    let path = this.dataService.getLearnTensorflowMetadata().getPartMetadata(prevPartId).path;
    return "/" + LearnTensorflowMetadata.getPath() + "/" + prevPartId + "/" + path + ".html";
  }

  public getShareUrl(network: string) {
    let currentUrl = window.location.href;
    let shareUrl = "";
    if (network == 'fb') {
      shareUrl = "https://www.facebook.com/dialog/share?app_id=449821918920615&display=popup&href=" + 
      currentUrl
    }

    if (network == 'twtr') {
      shareUrl = "https://twitter.com/intent/tweet?url=" + currentUrl + "&text=" + this.getTitle() + " - ";
    }

    if (network == 'mail') {
      shareUrl = "mailto:?body=" + this.getTitle() + "%0A%0A" + currentUrl + "%0A%0A";
    }

    return shareUrl;
  }

  public navigateToPostEditor() {
    this.router.navigateByUrl(ROUTE_LIVE_EDITOR); 
  }
}

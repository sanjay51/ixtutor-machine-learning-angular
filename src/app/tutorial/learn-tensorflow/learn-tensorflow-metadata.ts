import { Metadata } from '../../data-service/metadata';

export class LearnTensorflowMetadata implements Metadata {
    private static LEARN_TENSORFLOW_PATH = "learn-tensorflow"

    private title = "Learn Tensorflow by example"

    METADATA = {
        1: {
            "path": "introduction",
            "title": "Tensorflow Introduction",
            "highlight": false
        },

        2: {
            "path": "sessions",
            "title": "Tensorflow Sessions",
            "highlight": false
        },

        3: {
            "path": "graphs",
            "title": "Tensorflow Graphs",
            "highlight": false
        },

        4: {
            "path": "variable_sharing",
            "title": "Tensorflow Variables and Operations",
            "highlight": false
        },

        5: {
            "path": "linear_regression_with_tensorflow",
            "title": "Linear Regression with Tensorflow",
            "highlight": true
        },

        6: {
            "path": "training_neural_networks_with_tensorflow",
            "title": "Training Neural Networks with Tensorflow",
            "highlight": true
        },

        7: {
            "path": "transfer_learning_with_tensorflow_1",
            "title": "Transfer Learning with Tensorflow - I",
            "highlight": true
        },

        8: {
            "path": "transfer_learning_with_tensorflow_2_tf_hub",
            "title": "Transfer Learning with Tensorflow - II - Using modules from TF-Hub",
            "highlight": true
        }
    }

    getMaxPartId(): number {
        let arr = Object.keys(this.METADATA).map(x => +x);
        return arr[arr.length - 1];
    }

    getPartIds(): number[] {
        return Object.keys(this.METADATA).map(x => +x);
    }

    getTitle(): string {
        return this.title;
    }

    static getPath(): string {
        return LearnTensorflowMetadata.LEARN_TENSORFLOW_PATH;
    }

    getPartMetadata(partId: number) {
        return this.METADATA[partId];
    }
}
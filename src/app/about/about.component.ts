import { Component, OnInit } from '@angular/core';
import { Section } from '../section';
import { DataService } from '../data-service/data.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent extends Section implements OnInit {
  /* Inherited methods */
  getSectionId(): string {
    return "about"
  }
  getTitle(): string {
    return "About"
  }

  constructor(dataService: DataService) { super(dataService) }

  ngOnInit() {
  }
}

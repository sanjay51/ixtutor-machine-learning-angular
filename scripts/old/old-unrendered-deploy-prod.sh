echo "[STEP 1/4] Building (ng build --prod).." \
&&
ng build --prod \
&&
echo "[INFO] Build complete. 'dist' directory updated." \
&&
echo "[STEP 2/4] Copying files to ../deploy directory" \
&&
cp -rf dist/* src/.htaccess ../deploy/. \
&&
echo "[INFO] Copy complete." \
&&
cd ../deploy \
&&
git add * .htaccess \
&&
git commit -m "Update" \
&&
echo "[STEP 3/4] Uploading production distribution to remote git (bitbucket - git push)." \
&&
git push \
&&
echo "[INFO] Upload complete." \
&&
cd - \
&&
echo "[STEP 4/4] Pulling distribution on siteground host." \
&&
ssh ixtutor9@ixtutor.com -p18765 "cd www/ixtutor-prod/; git pull" \
&&
echo "[SUCCESS] ** Deployment complete **";


echo "[STEP 1/5] Building (npm run build-prod (instead of ng build --prod)).." \
&&
npm run build-prod \
&&
echo "[INFO] Build complete. 'dist' directory updated." \
&&
echo "[STEP 2/5] Creating new deployment" \
&&
rm -rf ../deploy \
&&
mkdir ../deploy \
&&
cd ../deploy \
&&
echo "[INFO] Cloning deployment git repo" \
&&
git clone git@bitbucket.org:sanjay51/deploy_ixtutor_machin_learning_angular.git \
&&
cd - \
&&
cp -rf dist/* ../deploy/deploy_ixtutor_machin_learning_angular/. \
&&
cd ../deploy/deploy_ixtutor_machin_learning_angular/ \
&&
echo "[INFO] Updating deploy repo with the changes" \
&&
git add * && git commit -m "Update" && git push \
&&
echo "[STEP 3/5] Deploying to http://beta.ixtutor.com.s3-website-us-east-1.amazonaws.com" \
&&
aws s3 sync . s3://beta.ixtutor.com --delete \
&&
echo "[INFO] Beta deployment complete." \
&&
echo -n "Deploy to prod (y/n)? " && read answer;

if [ $answer = 'y' ]
then 
    echo "[STEP 4/5] Deploying to PROD" \
    &&
    aws s3 sync . s3://ixtutor.com --delete \
    &&
    echo "[STEP 5/5] Creating Cloudfront invalidation" \
    &&
    aws cloudfront create-invalidation --distribution-id E3KUWILL4P6I6U --paths '/*';
fi;

cd - \
&&
echo "[SUCCESS] ** Deployment complete **" \
&&
echo "To get status, use:" \
&&
echo "aws cloudfront get-invalidation --distribution-id E3KUWILL4P6I6U --id | grep Status";